#pragma once

#include "Material.h"

namespace Library
{
	class Texture2D;
	class Mesh;
	class ContentManager;

	class MeshRenderingData final
	{
	public:
		MeshRenderingData(const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState = SamplerStates::TrilinearClamp);

		const ID3D11Buffer* VertexBuffer() const;
		const ID3D11Buffer* IndexBuffer() const;
		std::uint32_t IndexCount() const;

		Microsoft::WRL::ComPtr<ID3D11SamplerState> SamplerState() const;
		void SetSamplerState(const Microsoft::WRL::ComPtr<ID3D11SamplerState>& samplerState);

		std::shared_ptr<Texture2D> DiffuseMap() const;
		void SetDiffuseMap(const std::shared_ptr<Texture2D>& diffuseMap);

		void Initialize(ID3D11Device& device, const Mesh& mesh, const std::shared_ptr<Texture2D>& diffuseMap);
		void Initialize(ID3D11Device& device, const Mesh& mesh, ContentManager& content, const std::wstring& diffuseMapFilename);

	protected:
		Microsoft::WRL::ComPtr<ID3D11Buffer> mVertexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> mIndexBuffer;
		std::uint32_t mIndexCount;
		std::shared_ptr<Texture2D> mDiffuseMap;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> mSamplerState;
	};

	class MeshRenderingMaterial : public Material
	{
		RTTI_DECLARATIONS(MeshRenderingMaterial, Material)

	public:
		MeshRenderingMaterial(Game& game);
		MeshRenderingMaterial(const MeshRenderingMaterial&) = default;
		MeshRenderingMaterial& operator=(const MeshRenderingMaterial&) = default;
		MeshRenderingMaterial(MeshRenderingMaterial&&) = default;
		MeshRenderingMaterial& operator=(MeshRenderingMaterial&&) = default;
		virtual ~MeshRenderingMaterial() = default;		

		virtual std::uint32_t VertexSize() const override;
		virtual void Initialize() override;

		void UpdateConstantBuffer(DirectX::CXMMATRIX worldViewProjectionMatrix);

	private:
		virtual void BeginDraw() override;

		Microsoft::WRL::ComPtr<ID3D11Buffer> mConstantBuffer;		
	};
}