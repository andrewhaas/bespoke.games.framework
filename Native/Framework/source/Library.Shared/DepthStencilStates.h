#pragma once

#include <wrl.h>
#include <d3d11_2.h>

namespace Library
{
	class DepthStencilStates final
	{
	public:
		static Microsoft::WRL::ComPtr<ID3D11DepthStencilState> DefaultDepthCulling;
		static Microsoft::WRL::ComPtr<ID3D11DepthStencilState> NoDepthCulling;

		static void Initialize(ID3D11Device* direct3DDevice);
		static void Shutdown();

		DepthStencilStates() = delete;
		DepthStencilStates(const DepthStencilStates&) = delete;
		DepthStencilStates& operator=(const DepthStencilStates&) = delete;
		DepthStencilStates(DepthStencilStates&&) = delete;
		DepthStencilStates& operator=(DepthStencilStates&&) = delete;
		~DepthStencilStates() = default;
	};
}
